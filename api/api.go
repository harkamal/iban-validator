package api

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/harkamal/iban-validator/config"
)

// ApplyRoutes defines the routes
func applyAllRoutes() *mux.Router {
	r := mux.NewRouter()
	rPref := r.PathPrefix("/api").Subrouter()

	// Add route in api namespace
	rPref.HandleFunc("/iban/validate", ibanValidate).Methods("POST")

	// swagger doc route
	fs := http.FileServer(http.Dir("./swaggerui"))
	r.PathPrefix("/swaggerui/").Handler(http.StripPrefix("/swaggerui", fs))
	return r
}

// Start - listen and serve request
func Start() {
	router := applyAllRoutes()
	log.Println("Starting application on port", config.APIPort())
	log.Fatal(http.ListenAndServe(":"+config.APIPort(), router))
}
