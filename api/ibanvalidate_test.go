package api

import "testing"

// Test performValidations with inValid IBANs
func TestInvalidIBANsPerformValidations(t *testing.T) {
	invalidIBANs := []string{
		"1",
		"P",
		"PR",
		"PR098B789",
		"123456!7890",
		"AL35345678901234567890",
		"AL35345678901234567890",
		"AB35345678901234567890",
		"ATRO35345678901234567890",
	}

	// Loop over invalidIBANs, should raise an error
	for _, invalidIBAN := range invalidIBANs {
		ibanResponse := performValidations(invalidIBAN)
		if ibanResponse.Valid {
			// invalid IBAN did not raise an error,
			t.Errorf("invalid IBAN did not raise error: %v", ibanResponse.Message)
		}
	}
}

// Test performValidations with valid IBANs
func TestValidIBANsPerformValidations(t *testing.T) {
	validIBANs := []string{
		"GR9608100010000001234567890",
		"GI04BARC000001234567890",
		"DE75512108001245126199",
		"GE60NB0000000123456789",
		"FR7630006000011234567890189",
		"FI1410093000123458",
		"FO9264600123456789",
		"EE471000001020145685",
		"SV43ACAT00000000000000123123",
		"EG800002000156789012345180002",
		"DO22ACAU00000000000123456789",
		"DK9520000123456789",
		"CZ5508000000001234567899",
		"CY21002001950000357001234567",
		"HR1723600001101234565",
		"CR23015108410026012345",
		"BG18RZBB91550123456789",
		"BY86AKBB10100000002966000000",
		"AL35202111090000000001234567",
		"AD1400080001001234567890",
		"AT483200000012345864",
		"AZ96AZEJ00000000001234567890",
	}

	// Loop over validIBANs, should not raise an error
	for _, validIBAN := range validIBANs {
		ibanResponse := performValidations(validIBAN)
		if !ibanResponse.Valid {
			// valid IBAN raise an error,
			t.Errorf("valid IBAN raise error: %v", ibanResponse.Message)
		}
	}
}
