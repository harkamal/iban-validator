package api

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

var m *mux.Router
var req *http.Request
var err error
var respRec *httptest.ResponseRecorder

func init() {
	m = applyAllRoutes()
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	m.ServeHTTP(rr, req)

	return rr
}

// "POST" iban/validate request returns 200 ok.
func TestPostIbanValidate(t *testing.T) {
	req, err = http.NewRequest("POST", "/api/iban/validate", nil)
	if err != nil {
		t.Fatal("iban validate 'POST' request failed.")
	}
	response := executeRequest(req)

	if response.Code != http.StatusOK {
		t.Fatal("Server error: Returned ", response.Code, " instead of ", http.StatusOK)
	}
}

// Test ibanValidate response with valid IBANs
func TestPostIbanValidateWithValidIBANs(t *testing.T) {
	validIBANs := []string{
		"GR9608100010000001234567890",
		"GI04BARC000001234567890",
		"DE75512108001245126199",
		"GE60NB0000000123456789",
		"FR7630006000011234567890189",
		"FI1410093000123458",
		"FO9264600123456789",
		"EE471000001020145685",
		"SV43ACAT00000000000000123123",
	}

	var ibanResponse IbanValidateResponse
	// Loop over validIBANs, should not raise an error
	for _, validIBAN := range validIBANs {
		req, err = http.NewRequest("POST", "/api/iban/validate?iban="+validIBAN, nil)
		if err != nil {
			t.Fatal("iban validate 'POST' request failed.")
		}
		response := executeRequest(req)

		err := json.Unmarshal(response.Body.Bytes(), &ibanResponse)
		if err != nil {
			t.Fatal("error: failed to decode response body", err)
		}
		if !ibanResponse.Valid {
			t.Fatal("error: IBAN valid", ibanResponse.Valid, " instead of", !ibanResponse.Valid, ibanResponse.Message)
		}
	}
}

// Test ibanValidate response with unsupported countries
func TestPostIbanValidateWithUnSupportedCountries(t *testing.T) {
	validIBANs := []string{
		"PN9608100010000001234567890",
		"OP04BARC000001234567890",
		"PB75512108001245126199",
		"DL60NB0000000123456789",
	}

	var ibanResponse IbanValidateResponse
	// Loop over validIBANs, should not raise an error
	for _, validIBAN := range validIBANs {
		req, err = http.NewRequest("POST", "/api/iban/validate?iban="+validIBAN, nil)
		if err != nil {
			t.Fatal("iban validate 'POST' request failed.")
		}
		response := executeRequest(req)

		err := json.Unmarshal(response.Body.Bytes(), &ibanResponse)
		if err != nil {
			t.Fatal("error: failed to decode response body", err)
		}
		if ibanResponse.Status != http.StatusNotImplemented {
			t.Fatal("error: expected status", http.StatusNotImplemented, " instead of", ibanResponse.Status, "iban: ", validIBAN)
		}
	}
}

// "GET" request to 'undefined' route returns 404 not found.
func TestGet404(t *testing.T) {
	req, err = http.NewRequest("GET", "/undefined", nil)
	if err != nil {
		t.Fatal("undefined 'Get' request should not be a valid route.")
	}

	response := executeRequest(req)

	if response.Code != http.StatusNotFound {
		t.Fatal("Server error: Returned ", response.Code, " instead of ", http.StatusBadRequest)
	}
}
