package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"regexp"
	"strings"
	"unicode"

	"gitlab.com/harkamal/iban-validator/domain"
)

// ibanValidate request parameters
// swagger:parameters ibanValidate
type IbanValidateRequest struct {
	// provide IBAN number to validate
	// Required: true
	IBAN string `json:"iban"`
}

// swagger:response IbanValidateResponse
type IbanValidateResponse struct {
	// tells whether iban is valid or not
	Message string `json:"message"`
	// true if iban is valid otherwise false
	Valid bool `json:"valid"`
	// http status code
	Status int `json:"status"`
}

// swagger:route POST /api/iban/validate IBAN ibanValidate
//
// Validates IBAN.
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Schemes: http, https
//
//     responses:
//       200: IbanValidateResponse
func ibanValidate(w http.ResponseWriter, r *http.Request) {
	iban := r.URL.Query().Get("iban")
	w.Header().Add("Content-Type", "application/json")

	response := performValidations(iban)
	json.NewEncoder(w).Encode(response)
}

// performValidations - runs all the validations to validate IBAN
func performValidations(iban string) IbanValidateResponse {
	var validIBAN domain.IBAN
	// convert iban to uppercase.
	iban = strings.ToUpper(strings.Replace(iban, " ", "", -1))

	re := regexp.MustCompile(`^[0-9A-Z]*$`)
	if !re.MatchString(iban) {
		return buidResponse(false, "Only alphanumeric characters are allowed.", http.StatusBadRequest)
	}

	// start validating with the country code and suuported countries
	// which required first 2 characters atleast.
	if len(iban) < 2 {
		return buidResponse(false, "IBAN must include the country code and the required digits.", http.StatusBadRequest)
	}

	valid, err := validateSupportedCountry(iban[:2])
	if err != nil {
		return buidResponse(valid, fmt.Sprintf("Currently, we don't have support for country code %s.", iban[:2]), http.StatusNotImplemented)
	}
	// set validIBAN country code
	validIBAN.CountryCode = iban[:2]

	countrySettings := domain.IBANSupportedCountriesSettings[validIBAN.CountryCode]
	if countrySettings != nil && countrySettings.Length != len(iban) {
		return buidResponse(false, fmt.Sprintf("IBAN must have %d characters for country code %s given %d.", countrySettings.Length, iban[:2], len(iban)), http.StatusBadRequest)
	}

	valid, err = applyIBANValidationLogic(iban)
	if err != nil {
		return buidResponse(false, fmt.Sprintf("The check digits [%s] for this IBAN don't look right.", iban[2:4]), http.StatusOK)
	}

	return buidResponse(valid, "This IBAN looks right.", http.StatusOK)
}

// applyIBANValidationLogic apply rules to validate the format
// calculate the checksum as (97+1) minus the remainder
func applyIBANValidationLogic(iban string) (bool, error) {
	// move the first four digits of the IBAN to the end of the string.
	tmpIBAN := iban[4:] + iban[:4]
	// using the values in the conversion table, convert the country code, and any other letters, into numbers.
	convertedIBAN := ""
	var digitInString string
	for _, c := range tmpIBAN {
		if !unicode.IsDigit(c) {
			// Get character code point value
			digitInString = domain.IBANCharToDigitConversionTable[string(c)]
		} else {
			digitInString = string(c)
		}
		convertedIBAN += digitInString
	}

	// compute the Modulo 97-10 of this number
	// create bignum from mod string and perform module
	IBANBigVal, success := new(big.Int).SetString(convertedIBAN, 10)
	if !success {
		return false, errors.New("IBAN format is invalid")
	}

	mod := new(big.Int).SetInt64(97)
	result := new(big.Int).Mod(IBANBigVal, mod)

	// Check if module is equal to 1
	if result.Int64() != 1 {
		return false, errors.New("IBAN has incorrect check digits")
	}
	return true, nil
}

// validateSupportedCountry returns true if country is supported
func validateSupportedCountry(iban string) (bool, error) {
	if domain.IBANSupportedCountriesSettings[iban] == nil {
		return false, errors.New("not supported country")
	}
	return true, nil
}

// buidResponse returns the IbanValidateResponse
func buidResponse(valid bool, message string, status int) IbanValidateResponse {
	return IbanValidateResponse{
		Message: message,
		Valid:   valid,
		Status:  status,
	}
}
