package config

import "gitlab.com/harkamal/iban-validator/util"

func APIPort() string {
	return util.OptionalString("PORT", "8080")
}
