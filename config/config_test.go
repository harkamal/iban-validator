package config

import (
	"os"
	"testing"
)

// TestAPIPort returns the default port
func TestAPIPort(t *testing.T) {
	port := APIPort()
	if port != "8080" {
		t.Errorf("Port was incorrect, got: %s, want: %s.", port, "8080")
	}
}

// TestAPIPort returns the ENV port
func TestAPIPortWithEnvPORT(t *testing.T) {
	os.Setenv("PORT", "3000")
	port := APIPort()
	if port != "3000" {
		t.Errorf("Port was incorrect, got: %s, want: %s.", port, "3000")
	}
}
