package util

import (
	"os"
)

func OptionalString(name string, defaultValue string) string {
	val, ok := os.LookupEnv(name)
	if ok {
		return val
	}
	return defaultValue
}
