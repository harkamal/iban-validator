package util

import (
	"os"
	"testing"
)

// OptionalString returns the deafult value
func TestOptionalStringDefaultValue(t *testing.T) {
	port := OptionalString("PORT", "4000")
	if port != "4000" {
		t.Errorf("OptionalString returned port value is incorrect, got: %s, want: %s.", port, "4000")
	}
}

// OptionalString returns the ENV defined value
func TestOptionalString(t *testing.T) {
	os.Setenv("PORT", "3000")
	port := OptionalString("PORT", "4000")
	if port != "3000" {
		t.Errorf("OptionalString returned port value is incorrect, got: %s, want: %s.", port, "3000")
	}
}
