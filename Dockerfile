# Start from golang base image
FROM golang:1.18 AS builder

# Set the current working directory inside the container 
WORKDIR /app

# Copy go mod and sum files 
COPY go.mod go.sum ./


# Copy the source from the current directory to the working Directory inside the container 
COPY . .
RUN mkdir -p /app/swaggerui
COPY swaggerui /app/swaggerui

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -o main .

# Start a new stage from scratch
FROM alpine:latest

WORKDIR /root/

# Copy the Pre-built binary file from the previous stage. Observe we also copied the .env file
COPY --from=builder /app/main .
RUN mkdir -p mkdir ./swaggerui
COPY --from=builder /app/swaggerui ./swaggerui

# Expose port 8080 to the outside world
EXPOSE 8080

#Command to run the executable
CMD ["./main"]
