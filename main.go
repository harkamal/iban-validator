// Package classification IBAN Validator.
//
// ### The purpose of this application is to expose API to validate IBAN for different countries.
//
//     Schemes: http, https
//     Host: localhost:8080
//     Version: 1.0.0
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package main

import "gitlab.com/harkamal/iban-validator/api"

func main() {
	api.Start()
}
