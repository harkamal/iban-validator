# iban-validator

Micro service which validates IBAN (International Bank Account Number).

## Getting started

iban-validator is a Go project.

Package

The API uses "gorilla/mux" which implements a request router and dispatcher for matching incoming requests to their respective handler. See the [README for more](https://github.com/gorilla/mux/blob/master/README.md).

go-swagger is used for fancy swagger API documentation. Please have a look at go.mod file to know other libraries used in this project.

### API Documentation

Start the application and open localhost:8080/swaggerui/


### Project setup

**Prerequisites**

- Go 1.18

**Clone the repo**

Clone the repo

```bash
git clone https://gitlab.com/harkamal/iban-validator.git
```

### Compile and start

```bash
make run
```

### Run tests

```bash
go test ./...
```

## Running in Docker

Follow below steps to run the API and job processor insid docker.

```bash
git clone https://gitlab.com/harkamal/iban-validator.git
```

```bash
cd iban-validator
```

```bash
docker compose up
```

Access Swaggeer API documentation - http://localhost:8080/swaggerui/
Use swagger to validate IBAN
Verify using curl - 
```bash
curl -X POST "http://localhost:8080/api/iban/validate?iban=AL35202111090000000001234567" -H  "accept: application/json"
```

### Update swagger API documentation

```bash
make swagger
```

### Formatting

#### macOS:

```bash
gofmt -w `find . -type f -name "*.go"`
```

#### CentOS/RHEL:

```bash
gofmt -w `find . -type f -name "*.go"`
```