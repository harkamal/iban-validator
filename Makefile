check_install:
	which swagger || go get -u github.com/go-swagger/go-swagger/cmd/swagger
swagger: check_install
	swagger generate spec -o ./swaggerui/swagger.json

test: 
	go test ./...

run: 
	go run .

build:
	go build -o bin/main main.go